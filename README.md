### Prequisites and Dependencies


Right now, this runs only in Linux. It depends on the ngspice libraries and python's PySpice module.
To install, run the following in the terminal


`sudo apt install ngspice-deb libngspice0-dev`  
`pip install pyspice scipy`


### How to run


You can run the program from the command line. If you have the circuit stored in a file 'input.txt', and you want to find the equivalent resistance across the cell 'Vin', simply run it as:


`python r_cal.py input.txt Vin`
