from PySpice.Spice.Netlist import *
from scipy.stats import linregress
import sys

in_file = sys.argv[1]

file = open(in_file)
lines = file.readlines()
file.close()
cir = Circuit('Net Resistance')
for i in lines:
    i = i.strip().split()
    elem = i[0][0].upper()
    if elem == 'R':
        cir.R(i[0][1:], *i[1:])
        cir['R'+i[0][1:]].resistance = int(cir['R'+i[0][1:]].resistance)
    elif elem == 'V':
        cir.V(i[0][1:], *i[1:])
        cir['V'+i[0][1:]].dc_value = int(cir['V'+i[0][1:]].dc_value)
sim = cir.simulator()
for cell in sys.argv[2:]:
    code = 'analysis = sim.dc('+cell+'=slice(-5,5,0.1))'
    exec(code)
    V = analysis['v-sweep'].as_ndarray()
    I = analysis[cell].as_ndarray()
    R, c, r_value, p_value, std_err = linregress(I, V)
    print('Equivalent Resistance across '+cell+' is '+str(-R)+' ohm')
